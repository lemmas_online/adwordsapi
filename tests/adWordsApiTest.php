<?php

namespace leRisen\AdWordsAPI\Tests;

use PHPUnit\Framework\TestCase;

use leRisen\AdWordsAPI\Client;

class adWordsApiTest extends TestCase
{
	const CAMPAIGN_ID = '1341318691';
	const SECOND_CAMPAIGN_ID = '1342307670';
	
    const AD_ID = '261733644856';
	const AD_GROUP_ID = '53400473226';

	private $api;

	protected function setUp()
	{
		$this->api = new Client('tests/Auth.ini');
	}

	protected function tearDown()
	{
		$this->api = null;
	}
	
	public function testGetListAdvertisingCompanies()
	{
		$result = $this->api->listAdvertisingCompanies();

		var_dump($result);
	}
	
	public function testGetAdGroups()
	{
		$result = $this->api->adGroups(intval(self::CAMPAIGN_ID));

		var_dump($result);
	}
	
	public function testGetAdvertisementInGroup()
    {
        $result = $this->api->advertisementInGroup(intval(self::AD_GROUP_ID));

        var_dump($result);
    }
    
    public function testStopAds()
    {
        $result = $this->api->stopAds(intval(self::AD_GROUP_ID), [self::AD_ID]);

        var_dump($result);
    }
    
    public function testGetStatisticsAdvertisingCampaign()
    {
        $result = $this->api->statisticsAdvertisingCampaign(intval(self::CAMPAIGN_ID));

        var_dump($result);
    }
    
    public function testGetStatisticsAdvertisingCompanies()
    {
        $result = $this->api->statisticsAdvertisingCompanies(
            [intval(self::CAMPAIGN_ID), intval(self::SECOND_CAMPAIGN_ID)],
            '2018-03-28',
            '2018-04-04'
        );

        var_dump($result);
    }
}