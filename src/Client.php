<?php

namespace leRisen\AdWordsAPI;

use Laravie\Parser\Xml\Reader;
use Laravie\Parser\Xml\Document;

use Google\AdsApi\AdWords\AdWordsServices;
use Google\AdsApi\AdWords\AdWordsSession;

use Google\AdsApi\AdWords\Reporting\v201802\DownloadFormat;
use Google\AdsApi\AdWords\Reporting\v201802\ReportDefinition;
use Google\AdsApi\AdWords\Reporting\v201802\ReportDefinitionDateRangeType;
use Google\AdsApi\AdWords\Reporting\v201802\ReportDownloader;

use Google\AdsApi\AdWords\v201802\cm\{
    Ad, AdGroupAd, AdGroupService, AdGroupAdService, AdGroupCriterionService, CriterionType,
    AdGroupAdOperation, AdGroupAdStatus, CampaignService, CampaignStatus, DateRange,
    OrderBy, Paging, Predicate, PredicateOperator, ReportDefinitionReportType, Operator, Selector, SortOrder
};

use Google\AdsApi\AdWords\AdWordsSessionBuilder;
use Google\AdsApi\Common\OAuth2TokenBuilder;
use Google\AdsApi\AdWords\ReportSettingsBuilder;

class Client
{
    /**
     * Лимит
     */
    const PAGE_LIMIT = 500;
    
    /**
     * @var AdWordsSession|mixed
     */
    private $session;
    
    /**
     * @var AdWordsServices
     */
    private $adWordsServices;
    
    public function __construct($path)
    {
        $oAuth2Credential = (new OAuth2TokenBuilder())->fromFile($path)->build();
        $session = (new AdWordsSessionBuilder())->fromFile($path)->withOAuth2Credential($oAuth2Credential)->build();
        
        $this->session = $session;
        $this->adWordsServices = new AdWordsServices();
    }
    
    /**
     * Список рекламных кампаний (активных) название и id
     * @return array
     */
    public function listAdvertisingCompanies(): array
    {
        $result = array();
        
        $campaignService = $this->adWordsServices->get($this->session, CampaignService::class);
        
        $selector = new Selector();
        $selector->setFields(['Id', 'Name']);
        $selector->setOrdering([new OrderBy('Name', SortOrder::ASCENDING)]);
        $selector->setPredicates([new Predicate('Status', PredicateOperator::IN, [CampaignStatus::ENABLED])]);
        $selector->setPaging(new Paging(0, self::PAGE_LIMIT));
        
        $totalNumEntries = 0;
        
        do {
            $page = $campaignService->get($selector);
            
            if ($page->getEntries() !== null) {
                $totalNumEntries = $page->getTotalNumEntries();
                foreach ($page->getEntries() as $campaign) {
                    array_push($result, [
                        'id' => $campaign->getId(),
                        'name' => $campaign->getName()
                    ]);
                }
            }
            
            $selector->getPaging()->setStartIndex(
                $selector->getPaging()->getStartIndex() + self::PAGE_LIMIT
            );
        } while ($selector->getPaging()->getStartIndex() < $totalNumEntries);
        
        return $result;
    }
    
    /**
     * Статистика по всем или же выбранным кампаниям за заданный период по дням (клики, расход, показы)
     * @param  array $companies
     * @param  $startDate
     * @param  $endDate
     * @return array
     * @throws \Google\AdsApi\AdWords\v201802\cm\ApiException
     */
    public function statisticsAdvertisingCompanies(array $companies = [], $startDate, $endDate): array
    {
        $selector = new Selector();
        $selector->setFields(
            [
                'CampaignId',
                'Impressions',
                'Clicks',
                'Cost'
            ]
        );
        
        if (!empty($companies)) {
            $selector->setPredicates([new Predicate('CampaignId', PredicateOperator::IN, $companies)]);
        }
        
        $selector->setDateRange(
            new DateRange(
                date('Ymd', strtotime($startDate)),
                date('Ymd', strtotime($endDate))
            )
        );
        
        $reportDefinition = new ReportDefinition();
        $reportDefinition->setSelector($selector);
        $reportDefinition->setReportName('Статистика #' . uniqid());
        $reportDefinition->setDateRangeType(ReportDefinitionDateRangeType::CUSTOM_DATE);
        $reportDefinition->setReportType(ReportDefinitionReportType::CAMPAIGN_PERFORMANCE_REPORT);
        $reportDefinition->setDownloadFormat(DownloadFormat::XML);
        
        $reportDownloader = new ReportDownloader($this->session);
        
        $reportSettingsOverride = (new ReportSettingsBuilder())->includeZeroImpressions(true)->build();
        $reportDownloadResult = $reportDownloader->downloadReport($reportDefinition, $reportSettingsOverride);
        
        $path = sprintf('%s.xml', tempnam(sys_get_temp_dir(), 'stats-'));
        $reportDownloadResult->saveToFile($path);
        
        $xml = (new Reader(new Document()))->load($path);
        
        unlink($path);
        
        return $xml->parse([
            'companies' => ['uses' => 'table.row[::campaignID>id,::impressions,::clicks,::cost>amount_of_expenses]'],
        ]);
    }
    
    /**
     * Статистика по объявлениям конкретной рекламной кампании
     * @param  int $campaignId
     * @return array
     * @throws \Google\AdsApi\AdWords\v201802\cm\ApiException
     */
    public function statisticsAdvertisingCampaign(int $campaignId): array
    {
        $selector = new Selector();
        $selector->setFields(
            [
                'CampaignId',
                'Impressions',
                'Clicks',
                'Cost'
            ]
        );
        $selector->setPredicates([new Predicate('CampaignId', PredicateOperator::IN, [$campaignId])]);
        
        $reportDefinition = new ReportDefinition();
        $reportDefinition->setSelector($selector);
        $reportDefinition->setReportName('Статистика #' . uniqid());
        $reportDefinition->setDateRangeType(ReportDefinitionDateRangeType::LAST_7_DAYS);
        $reportDefinition->setReportType(ReportDefinitionReportType::CAMPAIGN_PERFORMANCE_REPORT);
        $reportDefinition->setDownloadFormat(DownloadFormat::XML);
        
        $reportDownloader = new ReportDownloader($this->session);
        
        $reportSettingsOverride = (new ReportSettingsBuilder())->includeZeroImpressions(true)->build();
        $reportDownloadResult = $reportDownloader->downloadReport($reportDefinition, $reportSettingsOverride);
        
        $path = sprintf('%s.xml', tempnam(sys_get_temp_dir(), 'stats-'));
        $reportDownloadResult->saveToFile($path);
        
        $xml = (new Reader(new Document()))->load($path);
        
        unlink($path);
        
        return $xml->parse([
            'id' => $campaignId,
            'amount_of_expenses' => ['uses' => 'table.row::cost'],
            'clicks' => ['uses' => 'table.row::clicks'],
            'impressions' => ['uses' => 'table.row::impressions']
        ]);
    }
    
    /**
     * Список групп объявлений
     * @param  int $campaignId
     * @return array
     */
    public function adGroups(int $campaignId): array
    {
        $result = array();
        
        $adGroupService = $this->adWordsServices->get($this->session, AdGroupService::class);
        
        $selector = new Selector();
        $selector->setFields(['Id', 'Name']);
        $selector->setOrdering([new OrderBy('Name', SortOrder::ASCENDING)]);
        $selector->setPredicates([new Predicate('CampaignId', PredicateOperator::IN, [$campaignId])]);
        $selector->setPaging(new Paging(0, self::PAGE_LIMIT));
        
        $totalNumEntries = 0;
        
        do {
            $page = $adGroupService->get($selector);
            
            if ($page->getEntries() !== null) {
                $totalNumEntries = $page->getTotalNumEntries();
                foreach ($page->getEntries() as $adGroup) {
                    array_push($result, [
                        'id' => $adGroup->getId(),
                        'name' => $adGroup->getName(),
                        'keywords' => $this->adGroupKeywords($adGroup->getId())
                    ]);
                }
            }
            
            $selector->getPaging()->setStartIndex(
                $selector->getPaging()->getStartIndex() + self::PAGE_LIMIT
            );
        } while ($selector->getPaging()->getStartIndex() < $totalNumEntries);
        
        return $result;
    }
    
    /**
     * Список ключей в определенной группе
     * @param  int $adGroupId
     * @return array
     */
    public function adGroupKeywords(int $adGroupId): array
    {
        $result = array();
        
        $adGroupCriterionService = $this->adWordsServices->get($this->session, AdGroupCriterionService::class);
        
        $selector = new Selector();
        $selector->setFields(['KeywordText']);
        $selector->setOrdering([new OrderBy('KeywordText', SortOrder::ASCENDING)]);
        $selector->setPredicates(
            [
                new Predicate('AdGroupId', PredicateOperator::IN, [$adGroupId]),
                new Predicate(
                    'CriteriaType',
                    PredicateOperator::IN,
                    [CriterionType::KEYWORD]
                )
            ]
        );
        $selector->setPaging(new Paging(0, self::PAGE_LIMIT));
        
        $totalNumEntries = 0;
        do {
            $page = $adGroupCriterionService->get($selector);
            
            if ($page->getEntries() !== null) {
                $totalNumEntries = $page->getTotalNumEntries();
                foreach ($page->getEntries() as $adGroupCriterion) {
                    array_push($result, $adGroupCriterion->getCriterion()->getText());
                }
            }
            
            $selector->getPaging()->setStartIndex(
                $selector->getPaging()->getStartIndex() + self::PAGE_LIMIT
            );
        } while ($selector->getPaging()->getStartIndex() < $totalNumEntries);
        
        return $result;
    }
    
    /**
     * Список объявлений в определенной группе
     * @param  int $adGroupId
     * @return array
     */
    public function advertisementInGroup(int $adGroupId): array
    {
        $result = array();
        
        $adGroupAdService = $this->adWordsServices->get($this->session, AdGroupAdService::class);
        
        $selector = new Selector();
        $selector->setFields(['Id', 'GmailTeaserHeadline', 'GmailTeaserDescription', 'GmailTeaserBusinessName']);
        $selector->setOrdering([new OrderBy('Id', SortOrder::ASCENDING)]);
        $selector->setPredicates([new Predicate('AdGroupId', PredicateOperator::IN, [$adGroupId])]);
        $selector->setPaging(new Paging(0, self::PAGE_LIMIT));
        
        $totalNumEntries = 0;
        do {
            $page = $adGroupAdService->get($selector);
            
            if ($page->getEntries() !== null) {
                $totalNumEntries = $page->getTotalNumEntries();
                foreach ($page->getEntries() as $adGroupAd) {
                    array_push($result, [
                        'id' => $adGroupAd->getAd()->getId(),
                        'name' => $adGroupAd->getAd()->getTeaser()->getHeadline(),
                        'description' => $adGroupAd->getAd()->getTeaser()->getDescription()
                    ]);
                }
            }
            
            $selector->getPaging()->setStartIndex(
                $selector->getPaging()->getStartIndex() + self::PAGE_LIMIT
            );
        } while ($selector->getPaging()->getStartIndex() < $totalNumEntries);
        
        return $result;
    }
    
    /**
     * Возможность остановки показа выбранных объявлений (массив id)
     * @param  int $adGroupId
     * @param  array $ads
     * @return bool
     */
    public function stopAds(int $adGroupId, array $ads = []): bool
    {
        if (!empty($ads)) {
            $adGroupAdService = $this->adWordsServices->get($this->session, AdGroupAdService::class);
            
            $operations = [];
            
            foreach ($ads as $adId) {
                $ad = new Ad();
                $ad->setId($adId);
                
                $adGroupAd = new AdGroupAd();
                $adGroupAd->setAdGroupId($adGroupId);
                $adGroupAd->setAd($ad);
                
                $adGroupAd->setStatus(AdGroupAdStatus::PAUSED);
                
                $operation = new AdGroupAdOperation();
                $operation->setOperand($adGroupAd);
                $operation->setOperator(Operator::SET);
                $operations[] = $operation;
            }
            
            $adGroupAdService->mutate($operations)->getValue()[0];
            
            return true;
        }
        
        return false;
    }
}